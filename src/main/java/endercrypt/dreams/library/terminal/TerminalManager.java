/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams.library.terminal;


import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.screen.Screen.RefreshType;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.TerminalFactory;
import com.googlecode.lanterna.terminal.ansi.UnixLikeTerminal.CtrlCBehaviour;
import com.googlecode.lanterna.terminal.swing.SwingTerminalFrame;

import endercrypt.dreams.application.Application;
import endercrypt.dreams.application.ExitCode;
import endercrypt.dreams.library.DreamsComponent;
import endercrypt.dreams.library.DreamsManager;


public class TerminalManager extends DreamsComponent
{
	private static final Logger logger = LogManager.getLogger(TerminalManager.class);
	
	public static TerminalManager connect(DreamsManager dreamsManager)
	{
		TerminalManager terminalManager = new TerminalManager(dreamsManager);
		terminalManager.start();
		return terminalManager;
	}
	
	private static final TerminalFactory terminalFactory = new DefaultTerminalFactory()
		.setUnixTerminalCtrlCBehaviour(CtrlCBehaviour.TRAP);
	
	private Terminal terminal;
	private TerminalScreen screen;
	private RefreshType refreshType = RefreshType.COMPLETE;
	
	private KeyboardCollector keyboardCollector;
	
	private TerminalManager(DreamsManager dreamsManager)
	{
		super(dreamsManager);
		
		logger.info("Constructing keyboard manager");
		
		// start terminal
		try
		{
			terminal = terminalFactory.createTerminal();
			logger.info("Initialized terminal " + terminal);
		}
		catch (IOException e)
		{
			logger.error("creating terminal failed", e);
			ExitCode.TERMINAL_ERROR.endApplication();
		}
		
		// configure gui terminal
		if (terminal instanceof SwingTerminalFrame)
		{
			logger.info("Detected " + SwingTerminalFrame.class.getSimpleName());
			logger.info("Making adjustments to Swing terminal ...");
			SwingTerminalFrame swingTerminalFrame = (SwingTerminalFrame) terminal;
			StringBuilder sb = new StringBuilder();
			sb.append(Application.name).append(" - ").append(Application.version);
			swingTerminalFrame.setTitle(sb.toString());
		}
		
		// keyboard
		logger.info("Setting up keyboard collector ...");
		keyboardCollector = new KeyboardCollector(terminal);
		
		// start screen
		try
		{
			screen = new TerminalScreen(terminal);
			screen.startScreen();
			logger.info("Initialized screen " + screen);
		}
		catch (IOException e)
		{
			logger.error("creating terminal screen failed", e);
			ExitCode.TERMINAL_ERROR.endApplication();
		}
		
		// clean
		clear();
	}
	
	private void start()
	{
		logger.info("Starting internal terminal manager systems");
		keyboardCollector.start();
	}
	
	public List<KeyStroke> collectKeyStrokes()
	{
		List<KeyStroke> keyStrokes = keyboardCollector.collect();
		
		for (KeyStroke key : keyStrokes)
		{
			if (key.isCtrlDown() && key.getCharacter() == 'c')
			{
				logger.info("Caught ctrl-c, terminating");
				ExitCode.SUCCESS.endApplication();
				break;
			}
		}
		
		return keyStrokes;
	}
	
	public void clear()
	{
		logger.trace("Clearing screen!");
		screen.setCursorPosition(null);
		screen.clear();
		refreshType = RefreshType.COMPLETE;
		render();
	}
	
	public boolean performResize()
	{
		boolean resized = screen.doResizeIfNecessary() != null;
		if (resized)
		{
			logger.debug("Terminal resized: " + screen.getTerminalSize());
		}
		refreshType = resized ? RefreshType.COMPLETE : RefreshType.DELTA;
		return resized;
	}
	
	public TextGraphics createGraphics()
	{
		return screen.newTextGraphics();
	}
	
	public void render()
	{
		try
		{
			logger.trace("Rendering ...");
			screen.refresh(refreshType);
			refreshType = RefreshType.DELTA;
		}
		catch (IOException e)
		{
			logger.error("refreshing terminal screen failed", e);
			ExitCode.TERMINAL_ERROR.endApplication();
		}
	}
	
	public void shutdown()
	{
		logger.info("Shutting down terminal manager!");
		
		// stop keyboard collector
		keyboardCollector.shutdown();
		
		// stop terminal
		try
		{
			screen.stopScreen();
		}
		catch (IOException e)
		{
			logger.error("stopping terminal screen failed", e);
			ExitCode.TERMINAL_ERROR.endApplication();
		}
	}
}
