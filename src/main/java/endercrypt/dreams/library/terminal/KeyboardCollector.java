/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams.library.terminal;


import java.io.IOException;
import java.util.ArrayDeque;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.terminal.Terminal;

import endercrypt.dreams.application.ExitCode;


public class KeyboardCollector
{
	private static final Logger logger = LogManager.getLogger(KeyboardCollector.class);
	
	private Thread thread = new Thread(this::thread);
	private Terminal terminal;
	private Queue<KeyStroke> keyStrokes = new ArrayDeque<>();
	
	public KeyboardCollector(Terminal terminal)
	{
		this.terminal = terminal;
	}
	
	public void start()
	{
		logger.info("starting keyboard collector thread!");
		thread.start();
	}
	
	private void thread()
	{
		try
		{
			while (Thread.currentThread().isInterrupted() == false)
			{
				KeyStroke key = terminal.readInput();
				logger.trace("Collected keystroke: " + key);
				synchronized (keyStrokes)
				{
					keyStrokes.add(key);
				}
			}
		}
		catch (IOException e)
		{
			logger.error("fetching keystrokes failed", e);
			ExitCode.TERMINAL_ERROR.endApplication();
		}
	}
	
	public List<KeyStroke> collect()
	{
		logger.debug("flushing keystrokes ...");
		synchronized (keyStrokes)
		{
			return keyStrokes.stream().collect(Collectors.toList());
		}
	}
	
	public void shutdown()
	{
		logger.info("Shutting down keyboard collector");
		thread.interrupt();
		thread = null;
	}
}
