/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams.library.activity;


import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.graphics.TextGraphics;

import endercrypt.dreams.application.ExitCode;
import endercrypt.dreams.library.DreamsComponent;
import endercrypt.dreams.library.DreamsManager;
import endercrypt.dreams.library.terminal.TerminalManager;


public class ActivityManager extends DreamsComponent
{
	private static final Logger logger = LogManager.getLogger(ActivityManager.class);
	
	public ActivityManager(DreamsManager dreamsManager)
	{
		super(dreamsManager);
	}
	
	private List<Activity> stack = new ArrayList<>();
	
	public Activity push(Activity activity)
	{
		logger.info("Activity got pushed: " + activity.getClass().getSimpleName());
		try
		{
			// initialize
			logger.debug("initializing activity");
			stack.add(activity);
			activity.initialize(dreamsManager);
			
			// start
			logger.debug("starting activity");
			activity.start();
			
			// main loop
			logger.debug("runnung activity main loop");
			TerminalManager terminalManager = dreamsManager.getTerminalManager();
			TextGraphics activityGraphics = terminalManager.createGraphics();
			while (activity.getLifeCycle() == ActivityLife.STARTED)
			{
				// keystrokes
				logger.trace("activity: key strokes");
				terminalManager.collectKeyStrokes().forEach(activity::triggerKey);
				
				// update
				logger.trace("activity: update");
				activity.update();
				
				// draw
				logger.trace("activity: draw");
				terminalManager.performResize();
				terminalManager.clear();
				draw(activityGraphics);
				terminalManager.render();
				
				// sleep
				logger.trace("activity: sleep");
				Thread.sleep(1000 / 20);
			}
			
			// finish
			logger.debug("activity " + activity.getClass().getSimpleName() + " finished");
			stack.remove(activity);
			return activity;
		}
		catch (ActivityException e)
		{
			panic(e.getActivity(), e);
			return null;
		}
		catch (Exception e)
		{
			panic(activity, e);
			return null;
		}
	}
	
	private void panic(Activity activity, Exception e)
	{
		logger.error("Caught exception in " + activity.getClass().getSimpleName(), e);
		ExitCode.ACTIVITY_FAILURE.endApplication();
	}
	
	private Stream<Activity> getDrawStack(TerminalSize terminalSize)
	{
		int i = stack.size();
		while (i > 0)
		{
			i--;
			Activity activity = stack.get(i);
			Rectangle drawRectangle = activity.getSize(terminalSize);
			if (drawRectangle.x != 0 || drawRectangle.y == 0 || drawRectangle.width != terminalSize.getColumns() || drawRectangle.height != terminalSize.getRows())
			{
				break;
			}
		}
		return stack.stream().skip(i);
	}
	
	public void draw(TextGraphics graphics)
	{
		TerminalSize terminalSize = graphics.getSize();
		getDrawStack(graphics.getSize()).forEach(new Consumer<Activity>()
		{
			@Override
			public void accept(Activity activity)
			{
				Rectangle drawRectangle = activity.getSize(terminalSize);
				TerminalPosition drawPosition = new TerminalPosition(drawRectangle.x, drawRectangle.y);
				TerminalSize drawSize = new TerminalSize(drawRectangle.width, drawRectangle.height);
				TextGraphics drawGraphics = graphics.newTextGraphics(drawPosition, drawSize);
				activity.draw(drawGraphics);
			}
		});
	}
}
