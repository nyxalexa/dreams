/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams.library.activity.event;


import java.util.HashMap;
import java.util.Map;

import endercrypt.dreams.library.activity.Activity;


public class ActivityEvents
{
	private Map<Class<? extends Activity>, ActivityEvents> activityClassEvents = new HashMap<>();
	
	public ActivityEvents get(Class<? extends Activity> activityClass)
	{
		ActivityEvents activityEvents = activityClassEvents.get(activityClass);
		if (activityEvents == null)
		{
			activityEvents = new ActivityEvents(activityClass);
			activityClassEvents.put(activityClass, activityEvents);
		}
		return activityEvents;
	}
	
	private ActivityEvents(Class<? extends Activity> activityClass)
	{
		// TODO Auto-generated constructor stub
	}
}
