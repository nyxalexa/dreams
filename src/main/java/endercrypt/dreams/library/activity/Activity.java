/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams.library.activity;


import java.awt.Rectangle;
import java.util.Objects;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;

import endercrypt.dreams.library.DreamsManager;


public abstract class Activity
{
	// DREAMS MANAGER //
	
	private DreamsManager dreamsManager;
	
	void initialize(DreamsManager dreamsManager)
	{
		Objects.requireNonNull(dreamsManager, "dreamsManager");
		switchLifeCycle(ActivityLife.UNBORN, ActivityLife.INITIALIZED);
		this.dreamsManager = dreamsManager;
	}
	
	protected DreamsManager getDreamsManager()
	{
		return dreamsManager;
	}
	
	// KEYSTROKES //
	
	void triggerKey(KeyStroke key)
	{
		// TODO: activity key stroke using events
	}
	
	// LIFECYCLE //
	
	private ActivityLife lifeCycle = ActivityLife.UNBORN;
	
	public ActivityLife getLifeCycle()
	{
		return lifeCycle;
	}
	
	private void switchLifeCycle(ActivityLife expected, ActivityLife target)
	{
		if (expected != null)
		{
			requireLifeCycle(expected);
		}
		lifeCycle = target;
	}
	
	private void requireLifeCycle(ActivityLife expected)
	{
		if (lifeCycle != expected)
		{
			panic("requireLifeCycle", new IllegalStateException("expected " + ActivityLife.class.getSimpleName() + "." + expected));
		}
	}
	
	private void panic(String method, Throwable e)
	{
		switchLifeCycle(null, ActivityLife.DEAD);
		throw new ActivityException(this, method, e);
	}
	
	// LIFETIME //
	private int lifetime = 0;
	
	public int getLifetime()
	{
		return lifetime;
	}
	
	// START //
	
	void start()
	{
		switchLifeCycle(ActivityLife.INITIALIZED, ActivityLife.STARTED);
		try
		{
			onStart();
		}
		catch (Exception e)
		{
			panic("start", e);
		}
	}
	
	protected void onStart()
	{
		
	}
	
	// DEATH //
	
	protected void die()
	{
		switchLifeCycle(ActivityLife.STARTED, ActivityLife.DEAD);
		try
		{
			onDeath();
		}
		catch (Exception e)
		{
			panic("death", e);
		}
	}
	
	protected void onDeath()
	{
		
	}
	
	// UPDATE //
	
	void update()
	{
		requireLifeCycle(ActivityLife.STARTED);
		lifetime++;
		try
		{
			onUpdate();
		}
		catch (Exception e)
		{
			panic("update", e);
		}
	}
	
	protected void onUpdate()
	{
		
	}
	
	// DRAW //
	
	void draw(TextGraphics graphics)
	{
		requireLifeCycle(ActivityLife.STARTED);
		try
		{
			onDraw(graphics);
		}
		catch (Exception e)
		{
			panic("draw", e);
		}
	}
	
	public Rectangle getSize(TerminalSize terminalSize)
	{
		return new Rectangle(terminalSize.getColumns(), terminalSize.getRows());
	}
	
	protected abstract void onDraw(TextGraphics graphics);
}
