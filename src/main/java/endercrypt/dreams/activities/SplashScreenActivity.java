/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams.activities;


import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.graphics.TextGraphics;

import endercrypt.dreams.application.Application;
import endercrypt.dreams.application.ExitCode;
import endercrypt.dreams.library.activity.Activity;


public class SplashScreenActivity extends Activity
{
	private static final String WORD = Application.name;
	private static final int SPEED = 3;
	
	private int index = -WORD.length();
	
	@Override
	protected void onUpdate()
	{
		index += SPEED;
	}
	
	@Override
	protected void onDraw(TextGraphics graphics)
	{
		TerminalSize size = graphics.getSize();
		boolean left = true;
		for (int y = 0; y < size.getRows(); y++)
		{
			int x = (left) ? (index) : (size.getColumns() - WORD.length() - index);
			
			graphics.putString(x, y, WORD);
			
			left = !left;
		}
		if (index >= size.getColumns())
		{
			getDreamsManager().getActivityManager().push(new LoginActivity());
			ExitCode.SUCCESS.endApplication();
		}
	}
}
